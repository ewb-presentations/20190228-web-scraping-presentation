# Relevant Python libraries. json handles the conversion of Python objects to JSON, requests
# provides the functionality to read web pages, and BeautifulSoup is our web scraping
# library
import json
import requests
from bs4 import BeautifulSoup

# A function to take in a dict of extra parameters and construct a URL for our site. 
# Additional parameters are passed in as {param: value}
def search_url(params=dict()):
  base_url = 'http://tn211.mycommunitypt.com/index.php/component/cpx/index.php'
  base_params = dict(
    task = 'search.query',
    advanced = 'true',
    geo_county = 'Shelby'
  )
  url_params = {**params, **base_params}
  s = base_url + '?' + '&'.join("{!s}={!s}".format(k, v) for (k, v) in url_params.items())
  return(s)

# Set the base URL (same as the one used in the search_url function), and 
# fetch the landing page HTML
url_stub = 'http://tn211.mycommunitypt.com/index.php/component/cpx/index.php'
url = search_url()
response = requests.get(url)

# Read in the landing page HTML for parsing
landing_page_soup = BeautifulSoup(response.text, 'html.parser')

# Get the page count
page_count = landing_page_soup.select('.page-links a:nth-last-child(2)')[0].string

# Get the list of links on the landing page
page_one_link_tags = landing_page_soup.select('p.resource-name a')
page_one_link_parts = [tag.get('href') for tag in page_one_link_tags]
page_one_link_urls = [url_stub + part for part in page_one_link_parts]

# Fetch the HTML for the second resource link and read in the HTML for parsing
page_one_link_two_url = page_one_link_urls[1]
page_one_link_two_response = requests.get(page_one_link_two_url)
page_one_link_two_soup = BeautifulSoup(page_one_link_two_response.text, 'html.parser')

# Extract the resource name
page_one_link_two_name = page_one_link_two_soup.select('h1#resource-name_top')[0].string

# Extract the resource description
page_one_link_two_description = page_one_link_two_soup.select('#view_field_description')[0].string

# Extract the resource phone numbers and labels
page_one_link_two_phone_label_tags = page_one_link_two_soup.select('#view_field_phonesLabel + div .row .col-sm-4 p:nth-child(1)')
page_one_link_two_phone_labels = [tag.string for tag in page_one_link_two_phone_label_tags]

page_one_link_two_phone_number_tags = page_one_link_two_soup.select('#view_field_phonesLabel + div .row .col-sm-8 p:nth-child(1)')
page_one_link_two_phone_numbers = [tag.string for tag in page_one_link_two_phone_number_tags]

# Extract the resournce contact names and email addresses
page_one_link_two_contact_name_tags = page_one_link_two_soup.select('#view_field_contactsLabel + div .row .col-sm-4 p:nth-child(1)')
page_one_link_two_contact_names = [tag.string for tag in page_one_link_two_contact_name_tags]

page_one_link_two_contact_email_tags = page_one_link_two_soup.select('#view_field_contactsLabel + div .row .col-sm-8 a')
page_one_link_two_contact_emails = [tag.string for tag in page_one_link_two_contact_email_tags]

# Extract the resource eligibility rules
page_one_link_two_eligibility = page_one_link_two_soup.select('p#view_field_eligibility')[0].string

# Combine the extracted information into a Python object
page_one_link_two_info = dict(
  name = page_one_link_two_name,
  phone_nums = dict(zip(page_one_link_two_phone_labels,
                        page_one_link_two_phone_numbers)),
  contacts = dict(zip(page_one_link_two_contact_names,
                      page_one_link_two_contact_emails)),
  eligibility = page_one_link_two_eligibility,
  description = page_one_link_two_description
)

# Convert the Python object to JSON and print it out
print(json.dumps(page_one_link_two_info, indent = 2))
